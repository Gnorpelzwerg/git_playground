from http.server import BaseHTTPRequestHandler, ThreadingHTTPServer
from urllib.parse import quote, unquote
import os
import json
import shutil
import sys
import io
from http import HTTPStatus


class Server( BaseHTTPRequestHandler ):
    """A basic file server with machine readable json
    """
    
    def do_GET( self ):
        
        f = self.send_head( )

        if f:

            try:

                shutil.copyfileobj( f, self.wfile )

            finally:

                f.close( )

    def send_head( self ):
        
        rel_path = unquote( self.path, errors = "surrogatepass" )
        path = os.path.abspath( "." + rel_path )

        f = None
        if os.path.isdir( path ):
           
           return self.list_directory( path, rel_path )

        try:

            f = open( path, 'rb' )

        except OSError:

            self.send_error( HTTPStatus.NOT_FOUND, "File not found" )
            return None

        try:

            fs = os.fstat( f.fileno( ))
            self.send_response( HTTPStatus.OK )
            self.send_header( "Content-type", "application/octet-stream" )
            self.send_header( "Content-Length", str( fs[ 6 ]))
            self.end_headers( )
            return f

        except:

            f.close( )
            raise

    def list_directory( self, path, rel_path ):

        info = { "children": [ ], "parent": os.path.dirname( rel_path )}
        enc = sys.getfilesystemencoding( )
        
        for name in os.listdir( path ):

            item_path = os.path.join( path, name )
            item = { }
            item[ "name" ] = name
            item[ "url" ] = quote( os.path.join( rel_path, name ), errors = "surrogatepass" ) 
            item[ "type" ] = "directory" if os.path.isdir( item_path ) else "file"
            info[ "children" ].append( item )

            if os.path.isfile( item_path ):

                item[ "size" ] = os.path.getsize( item_path )
          
        encoded = json.dumps( info, indent = 4, ensure_ascii = False ).encode( enc, "surrogateescape" )
        f = io.BytesIO( )
        f.write( encoded )
        f.seek( 0 )

        self.send_response( HTTPStatus.OK )
        self.send_header( "Content-type", "application/json; charset=%s" % enc )
        self.send_header( "Content-Length", str( len( encoded )))
        self.end_headers( )

        return f

server = ThreadingHTTPServer(( "", int( sys.argv[ 1 ])), Server )

try:
    server.serve_forever( )

except KeyboardInterrupt:
    pass

server.server_close( )
